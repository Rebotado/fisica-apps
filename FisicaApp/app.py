from flask import Flask, render_template, url_for, request
from models.vector import Vector
from models.projectile import Projectile
from models.inclined_plain import InclinedPlain
from models.electric_potencial import ElectricPotential
from models.momentum import Momentum, VectorM
from forms import(
    VectorOperationsForm,
    ProjectileForm,
    InclinedPlainForm,
    ElectricPotentialForm,
    MomentumForm,
)
from bokeh.plotting import figure, output_file, show
from bokeh.embed import components



app = Flask(__name__)
app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/potencial-electrico', methods=['GET','POST'])
def electric_potential():
    form = ElectricPotentialForm()
    result = 0
    script,div = ["",""]
    if request.method == 'POST':
        charge = ElectricPotential(form.charge_x.data, form.charge_y.data, form.charge.data, form.x.data, form.y.data)
        result = charge.get_electric_potential()
        script,div = charge.get_graph()

    return render_template('apps/electric_potential.html', form=form, result=result, script=script, div=div)

@app.route('/plano-inclinado', methods=['GET','POST'])
def inclined_plane():
    form = InclinedPlainForm()
    result = InclinedPlain(0,0)
    script,div = ["" , ""]
    if request.method == 'POST':
        mass = form.mass.data
        degree = form.degree.data
        result = InclinedPlain(mass, degree)
        script,div = result.get_graph()

    return render_template('apps/inclined_plane.html', form=form, result=result, script=script, div=div)

@app.route('/momento', methods=('GET','POST'))
def momentum():
    form = MomentumForm()
    result = ""
    vector1 = None
    vector2 = None
    script,div = ["",""]
    if request.method == 'POST':
        if form.vector_input.data == 'components':
            vector1_i = form.vector1_i.data
            vector1_j = form.vector1_j.data
            vector2_i = form.vector2_i.data
            vector2_j = form.vector2_j.data

            vector1 = VectorM(i_component=vector1_i, j_component=vector1_j)
            vector2 = VectorM(i_component=vector2_i, j_component=vector2_j)
        else:
            vector1_magnitude = form.vector1_magnitude.data
            vector1_degree = form.vector1_degree.data
            vector2_magnitude = form.vector2_magnitude.data
            vector2_degree = form.vector2_degree.data

            vector1 = VectorM(vector1_degree, vector1_magnitude)
            vector2 = VectorM(vector2_degree, vector2_magnitude)

        lever_length = form.lever_length.data
        lever_degree = form.lever_degree.data

        vector1_distance = form.vector1_distance.data
        vector2_distance = form.vector2_distance.data

        result = Momentum(lever_length, lever_degree, vector1, vector2, vector1_distance, vector2_distance)
        script , div = result.get_graph()
        result = result.momentum

    return render_template('apps/momentum.html', result=result, script=script, div=div, form=form)

@app.route('/movimiento-proyectil', methods=('GET','POST'))
def projectil_movement():
    form = ProjectileForm()
    result = Projectile(0,0)
    script,div = ["", ""]

    if request.method == 'POST':
        initial_velocity = form.initial_velocity.data
        exit_degree = form.exit_degree.data
        result = Projectile(exit_degree, initial_velocity)

        script,div = result.get_graph()

    result = str(result).split('\n')


    return render_template('apps/projectile_movement.html', form=form, result=result, script=script, div=div)


@app.route('/operaciones-vector', methods=('GET', 'POST'))
def vector_operations():
    form = VectorOperationsForm()
    result = Vector(0,0,0)
    if request.method == 'POST':
        # Pulls data from input
        vec1_i = form.vec1_i.data
        vec1_j = form.vec1_j.data
        vec1_k = form.vec1_k.data
        vec2_i = form.vec2_i.data
        vec2_j = form.vec2_j.data
        vec2_k = form.vec2_k.data
        operation = form.operations.data

        vector1 = Vector(vec1_i, vec1_j, vec1_k)
        vector2 = Vector(vec2_i, vec2_j, vec2_k)

        # calculator if statements
        if operation == 'add':
            result = vector1 + vector2
        elif operation == 'substract':
            result = vector1 - vector2
        elif operation == 'multiply':
            result = vector1 * vector2
        elif operation == 'product':
            result = vector1.cross_product(vector2)

    return render_template('apps/vector_operations.html', form=form, result=result)



if __name__ == "__main__":
    app.run(debug=True)