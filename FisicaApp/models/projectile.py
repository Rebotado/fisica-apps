import math
from bokeh.plotting import figure, output_file, show
from bokeh.embed import components
from bokeh.models import Range1d, Label


class Projectile:
    
    degree = 0
    magnitude = 0
    
    initial_x = 0
    initial_y = 0

    initial_velocity_x = 0
    initial_velocity_y = 0
    velocity_x = 0
    velocity_y = 0

    flight_time = 0
    
    x_acceleration = 0
    y_acceleration = 0

    x_displacement = 0
    y_displacement = 0

    maximum_height = 0
    time_maximum_height = 0
    range_maximum_height = 0
    x_displacement_maximum_height = 0
    y_displacement_maximum_height = 0
    

    range = 0



    def __init__(self, degree, magnitude):
        degree = math.radians(degree)
        self.degree = degree
        self.magnitude = magnitude
        self.x_acceleration = 0
        self.y_acceleration = -9.81
        self.initial_velocity_x = magnitude * math.cos(degree)
        self.initial_velocity_y = magnitude * math.sin(degree)


        self.flight_time = (2 * self.initial_velocity_y) / 9.81

        self.velocity_x = self.initial_velocity_x
        self.velocity_y = self.initial_velocity_y - (9.81 * self.flight_time)

        self.x_displacement = self.initial_velocity_x * self.flight_time
        self.y_displacement = (self.initial_velocity_y * self.flight_time) - (0.5 * 9.81 * pow(self.flight_time, 2))

        self.time_maximum_height = self.initial_velocity_y / 9.81

        self.x_displacement_maximum_height = self.initial_velocity_x * self.time_maximum_height
        self.y_displacement_maximum_height = (self.initial_velocity_y * self.time_maximum_height) - (0.5 * 9.81 * pow(self.time_maximum_height, 2))

        sin2 = lambda x: 0.5 * (1 - math.cos(2*x))
        self.maximum_height = (math.pow(magnitude,2) * sin2(degree)) / (2 * 9.81)

        self.range_maximum_height = self.initial_velocity_x * self.time_maximum_height
        self.range = (math.pow(magnitude, 2) * math.sin(2*degree)) / 9.81

        print(self)

    def __str__(self):
        return "Altura Maxima: ({0:.4f} , {1:.4f})\nTiempo al alcanzar altura maxima: {2:.4f}\nComponentes: {3:.4f}i , {4:.4f}j\nDesplacamiento: {5:.4f}".format(self.range_maximum_height, self.maximum_height, self.time_maximum_height, self.x_displacement_maximum_height, self.y_displacement_maximum_height, self.x_displacement)

    def trajectory(self, x):
        cos2 = lambda x: 0.5 * (1+math.cos(2*x))
        return (math.tan(self.degree) * self.x_displacement) - ( ((9.81)/(2 * pow(self.magnitude, 2) * cos2(x))) * math.pow(x,2))



    def get_graph(self):
        x , y = self._get_coordinates()
        x_max_height = [self.x_displacement_maximum_height , self.x_displacement_maximum_height]
        y_max_height = [0 , self.y_displacement_maximum_height]

        plot = figure(title = "Proyectil", 
        x_range=Range1d(start=0, end=self.x_displacement), 
        y_range=Range1d(start=0, end=self.y_displacement_maximum_height * 1.1))

        plot.line(x_max_height, y_max_height, line_color="yellow")
        plot.line(x,y, line_dash = [6,3])

        #labels
        max_height = Label(x=self.x_displacement_maximum_height * .82,
        y=self.y_displacement_maximum_height,
        text="Altura Maxima")

        plot.add_layout(max_height)

        script,div = components(plot)
        return script,div


    def _get_coordinates(self):
        x = []
        y = []
        coordinate = 0
        while coordinate < self.flight_time:
            x.append(self.initial_velocity_x * coordinate)
            y.append((self.initial_velocity_y * coordinate) - (0.5 * 9.81 * pow(coordinate, 2)))
            coordinate += 0.01
        return x , y




