import math
from bokeh.plotting import figure, output_file, show
from bokeh.embed import components
from bokeh.models import Range1d, Label, Arrow, OpenHead
from bokeh.models.markers import Square


class InclinedPlain:
    mass = 0
    degree = 0
    normal = 0
    acceleration = 0

    def __init__(self, mass, degree):
        degree = math.radians(degree)
        self.mass = mass
        self.degree = degree
        self.normal = mass * 9.81 * math.cos(degree)
        self.acceleration = 9.81 * math.sin(degree)

    def __str__(self):
        return "Normal: {0:.4f}\nAceleracion: {1:.4f}".format(self.normal, self.acceleration)
    
    def get_graph(self):

        cos = math.cos(self.degree)
        sin = math.sin(self.degree)

        cos_normal = math.cos(math.radians(90) + self.degree)
        sin_normal = math.sin(math.radians(90) + self.degree)

        cos_acc = math.cos(math.radians(180) + self.degree)
        sin_acc = math.sin(math.radians(180) + self.degree)

        x = [0, 20 * cos]
        y = [0, 20 * sin]

        x2 = [0, 20 * cos]
        y2 = [0, 0]

        x_offset = -1.2 if self.degree >= math.radians(45) else 0
        y_offset = 1.2 if self.degree < math.radians(45) else 0

        plot = figure(title='Plano inclinado', match_aspect=True)
        plot.line(x,y)
        plot.line(x2,y2)
        plot.square([(10 * cos) + x_offset] , [(10 * sin) + y_offset] , size=60, fill_alpha=0.2, angle=self.degree)
        plot.square([(10 * cos) + x_offset] , [(10 * sin) + y_offset] , size=1, fill_alpha=0.2, angle=self.degree)



        #normal vector
        x_normal = (3.5*cos_normal) + (10*cos) + x_offset
        y_normal = (3.5*sin_normal) + (10*sin) + y_offset

        normal = Arrow(end=OpenHead(), x_start=((10*cos) + x_offset), y_start=((10*sin) + y_offset), 
        x_end=x_normal, y_end=y_normal)


        #acceleration vector
        x_acc = (3.5*cos_acc) + (10*cos) + x_offset
        y_acc = (3.5*sin_acc) + (10*sin) + y_offset

        acc = Arrow(end=OpenHead(), x_start=((10*cos) + x_offset), y_start=((10*sin) + y_offset), 
        x_end=x_acc, y_end=y_acc)



        plot.add_layout(normal)
        plot.add_layout(acc)


        degree_label = Label(x=(3*cos), y=(1*sin), text='{0} \u00b0'.format(math.degrees(self.degree)))
        normal_label = Label(x=x_normal, y=y_normal, text='{:.2f}'.format(self.normal), angle=self.degree)
        acc_label = Label(x=x_acc - 1, y=y_acc - 1, text='{:.2f}'.format(self.acceleration), angle=self.degree)
        plot.add_layout(degree_label)
        plot.add_layout(normal_label)
        plot.add_layout(acc_label)


        script,div = components(plot)
        return script,div