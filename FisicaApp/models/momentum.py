import math

class VectorM:
    degree = 0
    magnitude = 0

    i_component = 0
    j_component = 0

    def __init__(self, degree=None, magnitude=None, i_component=None,j_component=None):
        if degree is None and i_component is not None:
            print('vector')
            magnitude = math.sqrt(math.pow(i_component,2) + math.pow(j_component,2))
            degree = math.degrees(math.atan(j_component/i_component))
        elif i_component is None and degree is not None:
            degree = math.radians(degree)
            i_component = magnitude * math.cos(degree)
            j_component = magnitude * math.sin(degree)

        self.degree = degree
        self.magnitude = magnitude
        self.i_component = i_component
        self.j_component = j_component



class Momentum:
    lever_length = 0
    lever_degree = 0

    force1 = VectorM()
    force2 = VectorM()

    force1_distance = 0
    force2_distance = 0

    momentum = 0

    


    def __init__(self, lever_length, lever_degree, force1, force2, force1_distance, force2_distance):
        self.lever_degree = lever_degree
        self.lever_length = lever_length
        self.force1 = force1
        self.force2 = force2
        self.force1_distance = force1_distance
        self.force2_distance = force2_distance

        print(force1.magnitude, force1.degree)

        self.momentum = (
            (self.force1.magnitude * math.sin(self.force1.degree) * self.force1_distance) + 
            (self.force2.magnitude * math.sin(self.force2.degree) * self.force2_distance)
        )



    def get_graph(self):
        return None,None

    


