class Vector:

    i = 0
    j = 0
    k = 0


    def __init__(self,i,j,k):
        self.i = i
        self.j = j
        self.k = k

    def __str__(self):
        return "{0}i , {1}j , {2}k".format(self.i, self.j, self.k)

    def __add__(self, other):
        i =  self.i + other.i
        j = self.j + other.j
        k = self.k + other.k
        vector = Vector(i,j,k)
        return vector

    def __sub__(self, other):
        i =  self.i - other.i
        j = self.j - other.j
        k = self.k - other.k
        vector = Vector(i,j,k)
        return vector

    def __mul__(self, other):
        i =  self.i * other.i
        j = self.j * other.j
        k = self.k * other.k
        vector = Vector(i,j,k)
        return vector

    def cross_product(self, other):
        i = (self.j * other.k) - (self.k * other.j)
        j = (self.k * other.i) - (self.i * other.k)
        k = (self.i * other.j) - (self.j * other.i)
        vector = Vector(i,j,k)
        return vector