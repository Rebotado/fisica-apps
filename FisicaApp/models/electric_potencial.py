import math
from bokeh.plotting import figure, output_file, show
from bokeh.embed import components
from bokeh.models import Range1d, Label, Arrow, OpenHead
from bokeh.models.markers import Square

class Charge:
    x = 0
    y = 0
    charge = 0

    def __init__(self, x=0, y=0, charge=0):
        self.x = x
        self.y = y
        self.charge = charge

class Coordinate:
    x = 0
    y = 0

    def __init__(self, x=0,y=0):
        self.x = x
        self.y = y


    def distance(self, x, y):
        return math.sqrt(pow(self.x - x,2) + pow(self.y - y,2))




class ElectricPotential:
    charge = Charge()
    coordinate = Coordinate()


    def __init__(self, x1_position, y1_position, charge, x2_position, y2_position):
        self.charge = Charge(x1_position, y1_position, charge)
        self.coordinate = Coordinate(x2_position, y2_position)
        print(charge)


    def get_electric_potential(self):
        return 9000000000 * (self.charge.charge / self.coordinate.distance(self.charge.x, self.charge.y))


    def get_graph(self):
        plot = figure(title="Potencial electrico", plot_height=400)
        plot.circle(self.charge.x, self.charge.y, size=30, fill_alpha=0.5)
        plot.circle(self.coordinate.x, self.coordinate.y, size=30, fill_alpha=0.2)
        x_line = [self.charge.x, self.coordinate.x]
        y_line = [self.charge.y, self.coordinate.y]
        plot.line(x=x_line, y=y_line, line_dash=[6,3])

        script,div = components(plot)

        return script,div