from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, SelectField, FloatField
from wtforms.validators import DataRequired, Length





VECTOR_OPERATIONS_CHOICES = (
    ('add' , 'Sumar'),
    ('substract', 'Restar'),
    ('multiply', 'Multiplicar'),
    ('product', 'Producto')
)

MOMENTUM_VECTOR_CHOICES = (
    ('components', 'Componentes'),
    ('vector' , 'Vector')
)

class VectorOperationsForm(FlaskForm):
    vec1_i = IntegerField('i')
    vec1_j = IntegerField('j')
    vec1_k = IntegerField('k')

    vec2_i = IntegerField('i')
    vec2_j = IntegerField('j')
    vec2_k = IntegerField('k')

    operations = SelectField('Operacion', choices=VECTOR_OPERATIONS_CHOICES)


class ProjectileForm(FlaskForm):
    initial_velocity = IntegerField('Velocidad inicial', validators=[DataRequired(message="Ingresa una velocidad inicial")])
    exit_degree = IntegerField('Angulo de salida', validators=[DataRequired(message="Ingresa un angulo inicial")])



class InclinedPlainForm(FlaskForm):
    mass = IntegerField('Masa', validators=[DataRequired(message="Ingresa una masa")])
    degree = IntegerField('Angulo', validators=[DataRequired(message="Ingresa un angulo")])

class ElectricPotentialForm(FlaskForm):
    charge = FloatField('Carga')
    charge_x = IntegerField('Coordenada X')
    charge_y = IntegerField('Coordenada Y')
    x = IntegerField('Coordenada X')
    y = IntegerField('Coordenada Y')

class MomentumForm(FlaskForm):
    vector_input = SelectField('Formato de vector', choices=MOMENTUM_VECTOR_CHOICES)

    vector1_magnitude = FloatField('Magnitud')
    vector1_degree = FloatField('Angulo')
    vector1_i = FloatField('i')
    vector1_j = FloatField('j')
    vector1_distance = FloatField('Distancia')

    vector2_magnitude = FloatField('Magnitud')
    vector2_degree = FloatField('Angulo')
    vector2_i = FloatField('i')
    vector2_j = FloatField('j')
    vector2_distance = FloatField('Distancia')

    lever_length = FloatField('Distancia')
    lever_degree = FloatField('Angulo')

